﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MessageReceiver
{
    class Program : Listener
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Старт программы");
            GetMessagesFromRabbit();          

        }
    }
}
