﻿using System;
using System.Text;
using Newtonsoft.Json;
using RabbitMQ.Client;

namespace _33_MessageBrokers
{
    internal class Receiver
    {        
        

        protected static void SendMessageToRabbit(object obj)
        {
            RabbitQueueSettings rqs = new RabbitQueueSettings();
            rqs.GetSettingsAsync();

            var factory = new ConnectionFactory()
            {
                HostName = rqs.HostName,
                VirtualHost = rqs.VirtualHost,
                UserName = rqs.UserName,
                Password = rqs.Passwords
            };
            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: "homework",
                        durable: true,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null
                    );
                    var user = GenerateUser;
                    string serializeMessage = JsonConvert.SerializeObject(user);
                    var body = Encoding.UTF8.GetBytes(serializeMessage);

                    channel.BasicPublish(exchange: "",
                        routingKey: "homework",
                        basicProperties: null,
                        body: body);
                    Console.WriteLine("[x] Sent {0} ", serializeMessage);
                }
            }
        }

        private static User GenerateUser
        {
            get
            {
                Random rnd = new Random();
                switch (rnd.Next(1, 5))
                {
                    case 1:
                        return new User() { Name = "Jonh", Age = 43, Email = "superman@contoso.com" };

                    case 2:
                        return new User() { Name = "Maria", Age = 53, Email = "unicorn@contoso.com" };

                    case 3:
                        return new User() { Name = "Sergio", Age = 31, Email = "fireman@contoso.com" };

                    case 4:
                        return new User() { Name = "unknownUser", Age = 67, Email = "" };

                    default:
                        return new User() { Name = "unknownUser", Age = 67, Email = "" };

                }
            }
        }
    }
}