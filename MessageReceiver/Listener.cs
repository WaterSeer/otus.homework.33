﻿using System;
using System.Text;
using System.Threading;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace MessageReceiver
{
    internal class Listener
    {
        protected static void GetMessagesFromRabbit()
        {
            RabbitQueueSettings rqs = new RabbitQueueSettings();
            rqs.GetSettingsAsync();


            
            var factory = new ConnectionFactory() { HostName = rqs.HostName, VirtualHost = rqs.VirtualHost, UserName = rqs.UserName, Password = rqs.Passwords };
            using (var connection = factory.CreateConnection())
            using (var channel = connection.CreateModel())
            {
                channel.QueueDeclare(queue: "homework",
                    durable: true,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                var consumer = new EventingBasicConsumer(channel);

                consumer.Received += (model, ea) =>
                {
                    var body = ea.Body;
                    var message = Encoding.UTF8.GetString(body);
                    var user = JsonConvert.DeserializeObject<User>(message);                    
                    Console.WriteLine(" [x] Received user:{0}", message);
                    if (user.Email != string.Empty)
                        channel.BasicAck(deliveryTag: ea.DeliveryTag, multiple: false);
                    else
                        channel.BasicNack(deliveryTag: ea.DeliveryTag, multiple: false, true);
                };
                channel.BasicConsume(queue: "homework",
                    autoAck: false,
                    consumer: consumer);                
                while (channel.IsOpen)
                {
                    Thread.Sleep(1000);                    
                }
            }
        }
    }
}