﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace _33_MessageBrokers
{
    public  class RabbitQueueSettings
    {
        public string HostName { get; set; } = "turkey.rmq.cloudamqp.com";
        public string VirtualHost { get; set; } = "fevikyqx";
        public string UserName { get; set; } = "fevikyqx";
        public string Passwords { get; set; } = "wB2XzHRkS3gAxcps3azZu6xsNYOykKDp";       

        public async Task<RabbitQueueSettings> GetSettingsAsync()
        {
            string jsonFileName = Directory.GetCurrentDirectory() + "/settings";
            using (FileStream fs = File.OpenRead(jsonFileName))
            {
                return await JsonSerializer.DeserializeAsync<RabbitQueueSettings>(fs);
            }
        }

        public async System.Threading.Tasks.Task SaveSettingsAsync(RabbitQueueSettings rqs)
        {
            string jsonFileName = Directory.GetCurrentDirectory() + "/settings";
            using (FileStream fs = File.Create(jsonFileName))
            {
                await JsonSerializer.SerializeAsync(fs, rqs);
            }
        }
    }
}
