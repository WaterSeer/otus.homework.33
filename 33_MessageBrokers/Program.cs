﻿using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;
using System.Threading;

namespace _33_MessageBrokers
{
    class Program : Receiver
    {
        static void Main(string[] args)
        {
            ConsoleKeyInfo key;
            System.Threading.Timer timerForSend = new System.Threading.Timer(new TimerCallback(SendMessageToRabbit), null, 0, 1000);
            Console.WriteLine("Нажмите Escape для останова передачи данных");
            do
            {
                key = Console.ReadKey();
                Console.WriteLine(key.Key + " клавиша была нажата");
            }
            while (key.Key != ConsoleKey.Escape);
            timerForSend.Dispose();
        }
    }
}
